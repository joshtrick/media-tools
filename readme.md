# Media Tools
## Introduction

- This repo is a collection of handy tools used to setup video services.
- The environment used for this repo is based on docker (ubuntu:18.04)
- Ths steps described below assumes that you have already installed docker on your computer

## Pull Ubuntu 18.04 Image

```
docker pull ubuntu:18.04
```

- After the image is pulled, run a container with at least 2 ports mapped to host and 1 shared volum.
- The sample steps below use 1935:1935 for rtmp, 8081:8081 for http and /home/corerain/share:/share for shared volume.

```
docker run --name sample-service \
           -p 1935:1935 \
           -p 8081:8081 \
           -v /etc/localtime:/etc/localtime:ro \
           -v /home/corerain/share:/share \
           -it ubuntu:18.04 \
           /bin/bash
```


## Install Media Libraries in Docker Container

### Change apt sources

```
cp /etc/apt/sources.list /etc/apt/sources.list.bak

echo "deb http://mirrors.aliyun.com/ubuntu/ bionic main restricted" > /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted" >> /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic universe" >> /etc/apt/sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic-updates universe" >> /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic multiverse" >> /etc/apt/sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic-updates multiverse" >> /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse" >> /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "deb http://security.ubuntu.com/ubuntu/ bionic-security main restricted" >> /etc/apt/sources.list
echo "deb http://security.ubuntu.com/ubuntu/ bionic-security universe" >> /etc/apt/sources.list
echo "deb http://security.ubuntu.com/ubuntu/ bionic-security multiverse" >> /etc/apt/sources.list

```

### Install utiliy tools, ffmpeg, gstreamer and nginx-dependencies

```
apt update; apt install \
      unzip \
      net-tools \
      curl \
      autoconf \
      automake \
      wget \
      zip \
      vim \
      mesa-common-dev \
      pkg-config \
      libtool \
      python3 \
      python3-dev \
      python3-pip

apt update; apt install ffmpeg

apt update; apt install \
      libgstreamer1.0-0 \
      gstreamer1.0-plugins-base \
      gstreamer1.0-plugins-good \
      gstreamer1.0-plugins-bad \
      gstreamer1.0-plugins-ugly \
      gstreamer1.0-libav \
      gstreamer1.0-doc \
      gstreamer1.0-tools \
      gstreamer1.0-x \
      gstreamer1.0-alsa \
      gstreamer1.0-gl \
      gstreamer1.0-gtk3 \
      gstreamer1.0-qt5 \
      gstreamer1.0-pulseaudio

apt update; apt install \
      libpcre3 \
      libpcre3-dev \
      openssl \
      libssl-dev \
      zlib1g-dev
```

### Install nginx

- You can find nginx packages needed in nginx/
- The nginx is by default installed in /usr/local/nginx.
- Assume you are currently in the root directory of this repo.

```
cd nginx/nginx-1.18.0
./configure --add-module=../nginx-http-flv-module-master
make
make install
cd ..
cp /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/nginx.conf.bak
cp nginx.conf /usr/local/nginx/conf/
/usr/local/nginx/sbin/nginx
```

### Pull from docker hub

All the packages described above have been installed and wrapped in docker image.

```
docker pull corerain/crmt:latest
```

## nginx Basics

### Start nginx

```
/usr/local/nginx/sbin/nginx
```

### Check nginx Conf.
```
/usr/local/nginx/sbin/nginx -t
```

### Reload nginx
```
/usr/local/nginx/sbin/nginx -s reload
```
### Basic Configurations

- http-flv

```
    http {
        ...
        server {
            listen 8081; #not default port 80
            ...

            location /flv {
                flv_live on;
            }
        }
    }
```

- hls
```
    http {
        ...
        server {
            listen 8081; #not default port 80
            ...

            location /hls {
                root /tmp/;
            }
        }
    }
```

- rtmp

```
    rtmp {
        ...
        server {
            listen 1935;
            ...

            application myapp {
                live on;
                hls on;
                hls_path /tmp/hls/;
            }
        }
    }
```

### Streaming

#### via RTMP

```
rtmp://example.com[:port]/appname/streamname
```

#### via HTTP-FLV

```
http://example.com[:port]/dir?[port=xxx&]app=appname&stream=streamname
```

#### via HLS
```
http://example.com[:port]/dir/streamname.m3u8
```

## Tools in This Repo.
- ff-file-hls-2-rtmp-loop.sh - An ffmpeg script to host a local video file or an hls video as a rtmp stream
- ff-record-video.sh - An ffmpeg script to record a video stream
- gst-rtmp-2-rtmp.sh - A gstreamer script to host a rtmp stream as multiple rtmp streams
- gst-rtsp-2-rtmp.sh - A gstreamer script to host a rsmp stream as multiple rtmp streams
- gst-video-2-png.sh - A gstreamer script to save picture (in png) from a video stream
- http-flv-player.html - A web page used to looked at http-flv video (rtmp from nginx)
