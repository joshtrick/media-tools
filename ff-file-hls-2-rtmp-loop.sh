#!/bin/bash
display_usage() {
  echo "Usage:"
  echo "bash $0 [OPTIONS]"
  echo ""
  echo "Options:"
  echo "  -h|--help                 Show help options"
  echo "  -i|--input  INFILE        Specify input file"
  echo "  -o|--output OUTRTMP       Specify output rtmp address"
  echo "  -crf        VALUE         Specify Constant Rate Factor (0-51)"
  echo "                            lossless: 0, worst quality: 51, default: 23"
}

# ********Default Parameters********
VIDEOSRC=\
/home/dummy/workspace/datasets/video/1080p_sample.mov
VIDEOSINK=\
rtmp://localhost/live/stream
CRF=\
23
# ********Default Parameters********

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -h|--help)
    display_usage
    exit 0
    ;;
    -i|--input)
    VIDEOSRC="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--output)
    VIDEOSINK="$2"
    shift # past argument
    shift # past value
    ;;
    -crf)
    CRF="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
esac
done

ffmpeg \
  -stream_loop -1 \
  -i $VIDEOSRC \
  -an \
  -crf $CRF \
  -c:v libx264 \
  -f flv \
  $VIDEOSINK


# Notes:
# Constant Rate Factor (CRF) value range: 0-51, 0 stands for lossless, default: 23
