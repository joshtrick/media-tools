#!/bin/bash
display_usage() {
  echo "Usage:"
  echo "bash $0 [OPTIONS]"
  echo ""
  echo "Options:"
  echo "  -h|--help                 Show help options"
  echo "  -i|--input  INRTSP        Specify input rtsp address"
  echo "  -o|--output OUTRTMP       Specify output rtmp address prefix"
  echo "  -n|--number OUTNUMBER     Specify number of output rtmp channels"
  echo "                            default: 1"
  echo "                            output address pattern: OUTRTMP_NUMBER"
  echo "                            NUMBER range: 1 to OUTNUMBER"
}

# ********Default Parameters********
VIDEOSRC=\
rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch36/main/av_stream
VIDEOSINK=\
rtmp://localhost/live/stream
SINKNUMBER=\
1
# ********Default Parameters********

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -h|--help)
    display_usage
    exit 0
    ;;
    -i|--input)
    VIDEOSRC="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--output)
    VIDEOSINK="$2"
    shift # past argument
    shift # past value
    ;;
    -n)
    SINKNUMBER="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
esac
done

CMD_COMPONENT="t. ! queue ! rtmpsink location="
for i in `seq 1 $SINKNUMBER`
do
  CMD_HOLDER=$CMD_HOLDER$CMD_COMPONENT$VIDEOSINK"_"$i" "
done

gst-launch-1.0 \
  rtspsrc location=$VIDEOSRC protocols=tcp \
  ! queue \
  ! rtph264depay \
  ! h264parse \
  ! flvmux streamable=true \
  ! tee name=t \
  $CMD_HOLDER
