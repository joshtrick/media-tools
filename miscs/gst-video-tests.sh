#!/bin/bash

#src_00="uridecodebin uri=rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch33/main/av_stream"
#src_01="uridecodebin uri=rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch34/main/av_stream"
#src_02="uridecodebin uri=rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch35/main/av_stream"
#src_03="uridecodebin uri=rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch36/main/av_stream"
#src_04="videotestsrc pattern=2"
#src_05="uridecodebin uri=rtsp://admin:letmein1@pd-rt-02.corerain.com:502/h264/ch33/main/av_stream"
#src_06="uridecodebin uri=rtsp://admin:letmein1@pd-rt-02.corerain.com:502/h264/ch34/main/av_stream"
#src_07="uridecodebin uri=rtsp://admin:letmein1@pd-rt-02.corerain.com:502/h264/ch35/main/av_stream"
#src_08="uridecodebin uri=rtsp://admin:letmein1@pd-rt-02.corerain.com:502/h264/ch36/main/av_stream"

src_00="uridecodebin uri=rtsp://admin:letmein1@pd-rt-02.corerain.com:502/h264/ch33/main/av_stream"
src_01="uridecodebin uri=rtsp://admin:letmein1@pd-rt-02.corerain.com:502/h264/ch34/main/av_stream"
src_02="uridecodebin uri=rtsp://admin:letmein1@pd-rt-02.corerain.com:502/h264/ch35/main/av_stream"
src_03="uridecodebin uri=rtsp://admin:letmein1@pd-rt-02.corerain.com:502/h264/ch36/main/av_stream"
src_04="videotestsrc pattern=2"
src_05="uridecodebin uri=rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch33/main/av_stream"
src_06="uridecodebin uri=rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch34/main/av_stream"
src_07="uridecodebin uri=rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch35/main/av_stream"
src_08="uridecodebin uri=rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch36/main/av_stream"

w=384
h=216

channels=0
if [[ $1 == 1 ]]
then
  channels=1
fi
if [[ $1 == 4 ]]
then
  channels=4
fi
if [[ $1 == 9 ]]
then
  channels=9
fi

(( w2=w*2 ))
(( h2=h*2 ))

(( w3=w*3 ))
(( h3=h*3 ))

if [[ $channels == 0 ]]
then
echo "To stream src_00:"
echo "bash video-tests.sh 1"
echo "To stream src_00~src_04:"
echo "bash video-tests.sh 4"
echo "To stream all 9 sources:"
echo "bash video-tests.sh 9"
echo ""
echo "Change src_0X variables as needed."
echo "Current sources settings:"
echo "src_00: $src_00"
echo "src_01: $src_01"
echo "src_02: $src_02"
echo "src_03: $src_03"
echo "src_04: $src_04"
echo "src_05: $src_05"
echo "src_06: $src_06"
echo "src_07: $src_07"
echo "src_08: $src_08"
echo ""
echo "Change w and h if you need other frame sizes."
echo "Current frame size: $w x $h (w x h)"
fi

if [[ $channels == 1 ]]
then
  gst-launch-1.0 \
    $src_00 ! videoscale ! video/x-raw,width=$w,height=$h ! videoconvert ! autovideosink
fi

if [[ $channels == 4 ]]
then
  gst-launch-1.0 \
    $src_00 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_0 \
    $src_01 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_1 \
    $src_02 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_2 \
    $src_03 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_3 \
    videomixer name=m \
    sink_0::xpos=0  sink_0::ypos=0 \
    sink_1::xpos=$w sink_1::ypos=0 \
    sink_2::xpos=0  sink_2::ypos=$h \
    sink_3::xpos=$w sink_3::ypos=$h \
    ! video/x-raw,width=$w2,height=$h2 ! autovideosink
fi

if [[ $channels == 9 ]]
then
  gst-launch-1.0 \
    $src_00 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_0 \
    $src_01 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_1 \
    $src_02 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_2 \
    $src_03 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_3 \
    $src_04 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_4 \
    $src_05 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_5 \
    $src_06 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_6 \
    $src_07 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_7 \
    $src_08 ! videoscale ! video/x-raw,width=$w,height=$h ! m.sink_8 \
    videomixer name=m \
    sink_0::xpos=0   sink_0::ypos=0 \
    sink_1::xpos=$w  sink_1::ypos=0 \
    sink_2::xpos=$w2 sink_2::ypos=0 \
    sink_3::xpos=0   sink_3::ypos=$h \
    sink_4::xpos=$w  sink_4::ypos=$h \
    sink_5::xpos=$w2 sink_5::ypos=$h \
    sink_6::xpos=0   sink_6::ypos=$h2 \
    sink_7::xpos=$w  sink_7::ypos=$h2 \
    sink_8::xpos=$w2 sink_8::ypos=$h2 \
    ! video/x-raw,width=$w3,height=$h3 ! autovideosink
fi
