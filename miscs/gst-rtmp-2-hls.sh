#!/bin/bash
display_usage() {
  echo "Usage:"
  echo "bash $0 [OPTIONS]"
  echo ""
  echo "Options:"
  echo "  -h|--help                 Show help options"
  echo "  -i|--input  INRTMP        Specify input rtmp address"
  echo "  -o|--output OUTHLS        Specify output hls address prefix"
  echo "  -n|--number OUTNUMBER     Specify number of output hls channels"
  echo "                            default: 1"
  echo "                            output address pattern"
  echo "                                           ts files: OUTHLS_NUMBER_seg_%05d.ts"
  echo "                                           playlist: OUTHLS_NUMBER.m3u8"
  echo "                            NUMBER range: 1 to OUTNUMBER"
}

# ********Default Parameters********
VIDEOSRC=\
rtmp://localhost/live/stream
VIDEOSINK=\
stream
SINKNUMBER=\
1
# ********Default Parameters********

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -h|--help)
    display_usage
    exit 0
    ;;
    -i|--input)
    VIDEOSRC="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--output)
    VIDEOSINK="$2"
    shift # past argument
    shift # past value
    ;;
    -n)
    SINKNUMBER="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
esac
done

CMD_COMPONENT="t. ! queue ! hlssink2 location="
for i in `seq 1 $SINKNUMBER`
do
  CMD_HOLDER=$CMD_HOLDER$CMD_COMPONENT$VIDEOSINK"_"$i"_seg_%05d.ts playlist-location="$VIDEOSINK"_"$i".m3u8 "
done

gst-launch-1.0 \
  rtmpsrc location=$VIDEOSRC \
  ! queue \
  ! flvdemux \
  ! queue \
  ! h264parse \
  ! tee name=t \
  $CMD_HOLDER
