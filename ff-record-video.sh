#!/bin/bash
display_usage() {
  echo "Usage:"
  echo "bash $0 [OPTIONS]"
  echo ""
  echo "Options:"
  echo "  -h|--help                 Show help options"
  echo "  -i|--input  INRTSP        Specify input rtsp address"
  echo "  -o|--output FILEPATH      Specify output file path and file name prefix"
  echo "  -t|--time   TIME          Specify time (in second) to record"
  echo "                            default: 1800"
  echo "  -dt         TIME          Specify time (in second) between two recordings"
  echo "                            default: 3600"
  echo "  -l|--loop   VALUE         Specify number of loops to run"
  echo "                            infinite loop: -1"
  echo "                            default: -1"
  echo "Time period definitions:"
  echo "  --midnight   VALUE        Time period: 0000-0400, activate: 1, deactivate: 0"
  echo "  --dawn       VALUE        Time period: 0400-0600, activate: 1, deactivate: 0"
  echo "  --morning    VALUE        Time period: 0600-0900, activate: 1, deactivate: 0"
  echo "  --midmorning VALUE        Time period: 0900-1200, activate: 1, deactivate: 0"
  echo "  --noon       VALUE        Time period: 1200-1400, activate: 1, deactivate: 0"
  echo "  --afternoon  VALUE        Time period: 1400-1800, activate: 1, deactivate: 0"
  echo "  --evening    VALUE        Time period: 1800-2100, activate: 1, deactivate: 0"
  echo "  --night      VALUE        Time period: 2100-2400, activate: 1, deactivate: 0"
  echo "                            default: 1"
}

# ********Default Parameters********
VIDEOSRC=\
rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch36/main/av_stream
VIDEOSINK=\
video
DURATION=\
1800
STOPTIME=\
3600
LOOP=\
-1
MIDNIGHT=\
1
DAWN=\
1
MORNING=\
1
MIDMORNING=\
1
NOON=\
1
AFTERNOON=\
1
EVENING=\
1
NIGHT=\
1
# ********Default Parameters********

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -h|--help)
    display_usage
    exit 0
    ;;
    -i|--input)
    VIDEOSRC="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--output)
    VIDEOSINK="$2"
    shift # past argument
    shift # past value
    ;;
    -t|--timeout)
    DURATION="$2"
    shift # past argument
    shift # past value
    ;;
    -dt)
    STOPTIME="$2"
    shift # past argument
    shift # past value
    ;;
    -l|--loop)
    LOOP="$2"
    shift # past argument
    shift # past value
    ;;
    --midnight)
    MIDNIGHT="$2"
    shift # past argument
    shift # past value
    ;;
    --dawn)
    DAWN="$2"
    shift # past argument
    shift # past value
    ;;
    --morning)
    MORNING="$2"
    shift # past argument
    shift # past value
    ;;
    --midmorning)
    MIDMORNING="$2"
    shift # past argument
    shift # past value
    ;;
    --noon)
    NOON="$2"
    shift # past argument
    shift # past value
    ;;
    --afternoon)
    AFTERNOON="$2"
    shift # past argument
    shift # past value
    ;;
    --evening)
    EVENING="$2"
    shift # past argument
    shift # past value
    ;;
    --night)
    NIGHT="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
esac
done

CNT=0
while true
do
  CURRENT_TIME=$(date "+%H%M")
  DO_RECORD=0
  if [ "$CURRENT_TIME" -ge 0000 ] && [ "$CURRENT_TIME" -lt 0400 ]
  then
    echo "MIDNIGHT"
    DO_RECORD=$MIDNIGHT
  fi
  if [ "$CURRENT_TIME" -ge 0400 ] && [ "$CURRENT_TIME" -lt 0600 ]
  then
    echo "DAWN"
    DO_RECORD=$DAWN
  fi
  if [ "$CURRENT_TIME" -ge 0600 ] && [ "$CURRENT_TIME" -lt 0900 ]
  then
    echo "MORNING"
    DO_RECORD=$MORNING
  fi
  if [ "$CURRENT_TIME" -ge 0900 ] && [ "$CURRENT_TIME" -lt 1200 ]
  then
    echo "MIDMORNING"
    DO_RECORD=$MIDMORNING
  fi
  if [ "$CURRENT_TIME" -ge 1200 ] && [ "$CURRENT_TIME" -lt 1400 ]
  then
    echo "NOON"
    DO_RECORD=$NOON
  fi
  if [ "$CURRENT_TIME" -ge 1400 ] && [ "$CURRENT_TIME" -lt 1800 ]
  then
    echo "AFTERNOON"
    DO_RECORD=$AFTERNOON
  fi
  if [ "$CURRENT_TIME" -ge 1800 ] && [ "$CURRENT_TIME" -lt 2100 ]
  then
    echo "EVENING"
    DO_RECORD=$EVENING
  fi
  if [ "$CURRENT_TIME" -ge 2100 ] && [ "$CURRENT_TIME" -lt 2400 ]
  then
    echo "NIGHT"
    DO_RECORD=$NIGHT
  fi

  if [ "$DO_RECORD" -eq 1 ]
  then
    echo "Time to work"
    ffmpeg \
      -t $DURATION \
      -rtsp_transport tcp \
      -i $VIDEOSRC \
      -an \
      -vcodec copy \
      $VIDEOSINK"_"$(date "+%Y%m%d-%H%M%S")".mp4"

    if [ "$LOOP" -gt 0 ]
    then
      CNT=$((CNT + 1))
      if [ "$CNT" -eq "$LOOP" ]
      then
        exit 0
      fi
    else
      echo "Infinite mode"
    fi

    sleep $STOPTIME
  else
    echo "Time to break"
    sleep 1
  fi
done
