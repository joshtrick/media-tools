#!/bin/bash
display_usage() {
  echo "Usage:"
  echo "bash $0 [OPTIONS]"
  echo ""
  echo "Options:"
  echo "  -h|--help                 Show help options"
  echo "  -i|--input  INRTSP        Specify input video address"
  echo "  -o|--output OUTIMG        Specify output images address prefix"
  echo "  -frn        NUMERATOR     Specify numerator of frame rate, default: 25"
  echo "  -frd        DENOMINATOR   Specify denominator of frame rate, default: 1"
  echo "                            NOTE: output fps=frn/frd"
}

# ********Default Parameters********
VIDEOSRC=\
rtsp://admin:letmein1@pd-rt-01.corerain.com:501/h264/ch36/main/av_stream
FILESINK=\
/home/dummy/share/images/pic
NUMERATOR=\
25
DENOMINATOR=\
1
# ********Default Parameters********

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -h|--help)
    display_usage
    exit 0
    ;;
    -i|--input)
    VIDEOSRC="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--output)
    FILESINK="$2"
    shift # past argument
    shift # past value
    ;;
    -frn)
    NUMERATOR="$2"
    shift # past argument
    shift # past value
    ;;
    -frd)
    DENOMINATOR="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
esac
done

CMD_HOLDER="! multifilesink location="$FILESINK"_%05d.png"

gst-launch-1.0 \
  uridecodebin uri=$VIDEOSRC \
  ! queue \
  ! videoconvert \
  ! videorate ! video/x-raw,framerate=$NUMERATOR/$DENOMINATOR \
  ! pngenc \
  ! queue \
  $CMD_HOLDER
